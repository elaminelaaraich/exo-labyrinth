package com.nespresso.sofa.recruitment.labyrinth;

public interface LabyrinthParser {

	LabyrinthStep parseLabyrinth(String currentLabyrinthStep);

	Room parseRoom(String currentRoom);

}
