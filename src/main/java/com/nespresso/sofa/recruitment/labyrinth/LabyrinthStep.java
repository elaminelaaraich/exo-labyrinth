package com.nespresso.sofa.recruitment.labyrinth;

import java.util.List;

public class LabyrinthStep {

	private Room startRoom;
	private Room nextRoom;
	private GateType gateType;
	private DoorState doorState;
	private boolean passed;

	public LabyrinthStep(Room startRoom, Room nextRoom, GateType gateType) {
		this.startRoom = startRoom;
		this.nextRoom = nextRoom;
		this.gateType = gateType;
		doorState = DoorState.Opened;
	}

	public boolean hasStartRoom(Room currentRoom) {

		return currentRoom.equals(startRoom) && isTheDoorOpen();
	}

	public void addNextRoom(List<Room> nextPossibleRooms) {
		nextPossibleRooms.add(nextRoom);
	}

	public boolean hasNextRoom(Room currentRoom) {
		return currentRoom.equals(nextRoom);
	}

	public void addStartRoom(List<Room> nextPssibleRooms) {
		nextPssibleRooms.add(startRoom);
	}

	public boolean contains(Room currentRoom, Room nextRoom2) {
		boolean containsCurrentRoom = currentRoom.equals(startRoom)
				|| (currentRoom.equals(nextRoom));
		boolean containsnextRoom2 = nextRoom2.equals(startRoom)
				|| (nextRoom2.equals(nextRoom));

		if (containsCurrentRoom && containsnextRoom2) {
			this.passed = true;
			return true;
		}
		return false;
	}

	private boolean isTheDoorOpen() {
		return doorState.equals(DoorState.Opened);
	}

	public void closeDoor() {
		if (!isTheDoorOpen()) {
			throw new DoorAlreadyClosedException();
		}
		doorState = DoorState.Closed;
	}

	public boolean testIfTheDoorClosed() {
		if (!isTheDoorOpen()) {
			return true;
		}
		return false;
	}

	public String reportSensor() {
		if (this.passed) {
			return gateType.report(startRoom, nextRoom);
		}
		return "";
	}

}
