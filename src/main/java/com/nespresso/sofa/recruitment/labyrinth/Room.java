package com.nespresso.sofa.recruitment.labyrinth;
 
public class Room {

	private char name;

	public Room(char name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Room) {
			return ((Room) obj).name == this.name;
		}
		return false;
	}
	//For My test
	@Override
	public String toString() {
		return name+"";
	}
 
}
