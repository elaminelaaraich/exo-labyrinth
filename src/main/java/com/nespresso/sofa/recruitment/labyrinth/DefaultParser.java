package com.nespresso.sofa.recruitment.labyrinth;

public class DefaultParser implements LabyrinthParser {

	@Override
	public LabyrinthStep parseLabyrinth(String currentLabyrinthStep) {
		char startRoomName = currentLabyrinthStep.charAt(0);
		char nextRoomName = currentLabyrinthStep.charAt(2);
		char gateTypeChar = currentLabyrinthStep.charAt(1);
		LabyrinthStep labyrinthStep = LabyrinthStepFactory.createLabyrinthStep(
				startRoomName, nextRoomName, gateTypeChar);
		return labyrinthStep;
	}

	@Override
	public Room parseRoom(String currentRoom) {
		char currentRoomChar = currentRoom.charAt(0);
		return new Room(currentRoomChar);
	}

}
