package com.nespresso.sofa.recruitment.labyrinth;

public class Labyrinth {
	// List<LabyrinthStep> labyrinthSteps = new ArrayList<LabyrinthStep>();
	private LabyrinthSteps labyrinthSteps;
	private Room currentRoom;

	public Labyrinth(String... labyrinthStepsEntry) {
		labyrinthSteps = new LabyrinthSteps();
		LabyrinthParser parser = getParser();
		for (String currentLabyrinthStep : labyrinthStepsEntry) {
			LabyrinthStep labyrinthStep = parser
					.parseLabyrinth(currentLabyrinthStep);
			labyrinthSteps.addLabyrinthStep(labyrinthStep);
		}
	}

	public void popIn(String room) {
		LabyrinthParser parser = getParser();
		currentRoom = parser.parseRoom(room);
	}

	public void walkTo(String room) {
		LabyrinthParser parser = getParser();
		Room nextRoom = parser.parseRoom(room);
		if (labyrinthSteps.canWalk(currentRoom, nextRoom)) {
			currentRoom = nextRoom;
		} else
			throw new IllegalMoveException();
	}

	public void closeLastDoor() {
		labyrinthSteps.closeLastDoor(); 
	}

	public String readSensors() {
		return labyrinthSteps.readSensors();
	}

	private DefaultParser getParser() {
		return new DefaultParser();
	}

	public String getCurrentPosition() {
		return currentRoom.toString();
	}
}
