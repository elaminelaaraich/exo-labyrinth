package com.nespresso.sofa.recruitment.labyrinth;

public class LabyrinthStepFactory {

	public static LabyrinthStep createLabyrinthStep(char startRoomName,
			char nextRoomName, char gateTypeChar) {
		Room startRoom = new Room(startRoomName);
		Room nextRoom = new Room(nextRoomName);
		boolean isGateIsNormal = gateTypeChar=='|';
		GateType gateType = isGateIsNormal ? GateType.Normal
				: GateType.WithSensor; 
		return new LabyrinthStep(startRoom, nextRoom, gateType);
	}

}
