package com.nespresso.sofa.recruitment.labyrinth;

public enum GateType {
	Normal {
		@Override
		public String report(Room startRoom, Room nextRoom) { 
			return "";
		}
	},
	WithSensor {
		@Override
		public String report(Room startRoom, Room nextRoom) {
			return startRoom + "" + nextRoom;
		}
	};

	public abstract String report(Room startRoom, Room nextRoom);

}
