package com.nespresso.sofa.recruitment.labyrinth;

import java.util.ArrayList;
import java.util.List;

public class LabyrinthSteps {

	private List<LabyrinthStep> labyrinthSteps = new ArrayList<LabyrinthStep>();
	private LabyrinthStep lastLabyrinthStep;

	public LabyrinthSteps() {
	}

	public void addLabyrinthStep(LabyrinthStep labyrinthStep) {
		labyrinthSteps.add(labyrinthStep);
	}

	public boolean canWalk(Room currentRoom, Room nextRoom) {
		List<LabyrinthStep> labyrinthStepList = getLabyrinthStepContains(
				currentRoom, nextRoom);

		boolean canWolk = labyrinthStepList.size() != 0;

		identifyLastLabyrinthStep(currentRoom, nextRoom, canWolk);
		if (lastLabyrinthStep.testIfTheDoorClosed())
			throw new ClosedDoorException();
		return canWolk;
	}

	private List<LabyrinthStep> getLabyrinthStepContains(Room currentRoom,
			Room nextRoom) {
		List<LabyrinthStep> labyrinthStepList = new ArrayList<LabyrinthStep>();
		for (LabyrinthStep currentLabyrinthStep : labyrinthSteps) {
			if (currentLabyrinthStep.contains(currentRoom, nextRoom)) {
				labyrinthStepList.add(currentLabyrinthStep);
			}
		}
		return labyrinthStepList;
	}

	private void identifyLastLabyrinthStep(Room currentRoom, Room nextRoom,
			boolean canWolk) {
		if (canWolk) {
			lastLabyrinthStep = getLabyrinthStepThatContains(currentRoom,
					nextRoom);

		}
	}

	private LabyrinthStep getLabyrinthStepThatContains(Room currentRoom,
			Room nextRoom) {

		LabyrinthStep labyrinthStep = null;
		for (LabyrinthStep currentLabyrinthStep : labyrinthSteps) {
			if (currentLabyrinthStep.contains(currentRoom, nextRoom)) {

				labyrinthStep = currentLabyrinthStep;
				break;
			}
		}

		return labyrinthStep;
	}

	public void closeLastDoor() {
		int lastLabyrinthStepIndex = labyrinthSteps.indexOf(lastLabyrinthStep);
		lastLabyrinthStep.closeDoor();
		labyrinthSteps.set(lastLabyrinthStepIndex, lastLabyrinthStep);

	}

	public String readSensors() {
		StringBuilder builder = new StringBuilder();
		for (LabyrinthStep currentLabyrinthStep : labyrinthSteps) {
			String currentSensor = currentLabyrinthStep.reportSensor();
			if (!builder.toString().contains(currentSensor)) {
				if (currentSensor.length() != 0)
					builder.append(";");
				builder.append(currentSensor);
			}
		}
		return builder.substring(1).toString();
	}

}
